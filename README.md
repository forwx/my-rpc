> 实现自己的RPC框架，参考视频：[自己动手实现RPC框架](https://www.imooc.com/learn/1158)
### 总结：
> 1. 首先要先明确RPC需要用到哪些模块? 
> 2. 协议模块(proto)
> 3. 序列化模块(codec)
> 4. 网络模块(transport)
> 5. server模块(server)
> 6. client模块(client)
> 7. 其次重点要了解**Jetty**,以及动态代理

### 各个模块的实现
1. 协议模块 
   1. 网络传输的端点(**Peer**)
   2. RPC的一次请求(**Request**)
   3. RPC的一次响应(**Response**)
   4. 服务(**ServiceDescriptor  : 相当于就是本地的一个类的方法的所有信息**)
2. 序列化模块(**采用的是FastJson的Json序列化实现**)
   1. 序列化(**Encoder**)
   2. 反序列化(**Decoder**)
3. 网络模块(transport)(**重点**)
   1. 网络传输的客户端(**HttpTransportClient**)
      1. 创建连接(**connect**)
      2. 发送数据，并且等待响应(**write**)
      3. 关闭连接(**close**)
   2. 网络传输的服务端(**HttpTransportServer**)
      1. 启动，监听(**init**)
      2. 接受请求(**start**)
      3. 关闭监听(**stop**)
   3. 请求处理器(**RequestHandler**)
4. Server模块
   1. Rpc服务配置(**RpcServerConfig**)
      1. 网络模块的服务端(**TransportServer**)
      2. 序列化(**Encoder**)
      3. 反序列化(**Decoder**)
      4. 端口(**port**)
   2. 服务实例(**ServerInstance**)
      1. 目标类(**Object**)
      2. 方法(**Method**)
   3. 调用具体服务(**ServerInvoker**)
   4. 服务管理(**ServerManager**)
      1. 将服务注册进去
      2. 查看服务
   5. RpcServer(**整合上述内容**)
5. Client模块
   1. Rpc服务配置(**RpcClientConfig**)
      1. 网络模块的服务端(**TransportServer**)
      2. 序列化(**Encoder**)
      3. 反序列化(**Decoder**)
      4. 连接数(**Connect**)
      5. 网络轮询(**TransportSelector : 表示选择哪个server去连接**)
      6. 调用服务端的端点(**servers**)
   2. 网络轮询(**TransportSelector**)
      1. 初始化(**init**)
      2. 选择(**select**)
      3. 释放资源(**release : 选择一个transport与server做交互**)
      4. 关闭(**close**)
   3. 远程服务的代理类(**RemoteInvoker**)
   4. RpcClient(**整合上述内容**)
   