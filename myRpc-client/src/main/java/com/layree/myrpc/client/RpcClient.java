package com.layree.myrpc.client;

import com.layree.codec.Decoder;
import com.layree.codec.Encoder;
import com.layree.myrpc.common.utils.ReflectionUtils;

import java.lang.reflect.Proxy;

/**
 * @ClassName: RpcClient
 * @Author: lin rui
 * @Date: 2023/5/4 17:26
 * @Description:
 */
public class RpcClient {
    private RpcClientConfig config;
    private Encoder encoder;
    private Decoder decoder;
    private TransportSelector selector;

    public RpcClient() {
        this(new RpcClientConfig());
    }

    public RpcClient(RpcClientConfig config) {
        this.config = config;
        this.encoder = ReflectionUtils.newInstance(this.config.getEncoderClass());
        this.decoder = ReflectionUtils.newInstance(this.config.getDecoderClass());
        this.selector = ReflectionUtils.newInstance(this.config.getSelectorClass());

        this.selector.init(
                this.config.getServers(),
                this.config.getConnection(),
                this.config.getTransportClass()
        );
    }


    public <T> T getProxy(Class<T> clazz){
        return (T) Proxy.newProxyInstance(
                getClass().getClassLoader(),
                new Class[]{clazz},
                new RemoteInvoker(clazz,encoder,decoder,selector));
    }
}
