package com.layree.myrpc.client;

import com.layree.codec.Decoder;
import com.layree.codec.Encoder;
import com.layree.codec.JSONDecoder;
import com.layree.codec.JSONEncoder;
import com.layree.myrpc.proto.Peer;
import com.layree.myrpc.transport.HttpTransportClient;
import com.layree.myrpc.transport.TransportClient;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName: RpcClientConfig
 * @Author: lin rui
 * @Date: 2023/5/4 17:12
 * @Description:
 */
@Data
public class RpcClientConfig {
    private Class<? extends TransportClient> transportClass = HttpTransportClient.class;
    private Class<? extends Encoder> encoderClass = JSONEncoder.class;
    private Class<? extends Decoder> decoderClass = JSONDecoder.class;
    private Class<? extends TransportSelector> selectorClass = RandomTransportSelector.class;
    private int connection = 1;
    private List<Peer> servers = Arrays.asList(new Peer("127.0.0.1",3000));
}
