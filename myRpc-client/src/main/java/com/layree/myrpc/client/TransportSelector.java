package com.layree.myrpc.client;

import com.layree.myrpc.proto.Peer;
import com.layree.myrpc.transport.TransportClient;

import java.util.List;

/**
 * @InterfaceName: TransportSelector
 * @Author: lin rui
 * @Date: 2023/5/4 16:28
 * @Description:  表示选择哪个server去连接
 */
public interface TransportSelector {
    /**
     * 初始化selector
     * @param peers 可以连接的server端点信息
     * @param count client与server建立多少个连接
     * @param clazz client实现class
     */
    void init(List<Peer> peers, int count, Class<? extends TransportClient> clazz);
    /**
     * 选择一个transport与server做交互
     * @return  网络client
     */
    TransportClient select();

    /**
     * 释放资源
     * @param client
     */
    void release(TransportClient client);
    void close();

}
