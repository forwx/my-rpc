package com.layree.codec;

/**
 * @InterfaceName: Decode
 * @Author: lin rui
 * @Date: 2023/5/3 21:44
 * @Description:
 */
public interface Decoder {
    <T> T decode(byte[] bytes, Class<T> clazz);
}
