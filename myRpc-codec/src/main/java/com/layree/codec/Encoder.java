package com.layree.codec;

/**
 * @InterfaceName: Encoder
 * @Author: lin rui
 * @Date: 2023/5/3 21:43
 * @Description:
 */
public interface Encoder {
    byte[] encode(Object obj);
}
