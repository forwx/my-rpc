package com.layree.codec;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName: JSONDecoder
 * @Author: lin rui
 * @Date: 2023/5/3 21:49
 * @Description:  基于json的反序列化实现
 */
public class JSONDecoder implements Decoder{
    @Override
    public <T> T decode(byte[] bytes, Class<T> clazz) {
        return JSON.parseObject(bytes, clazz);
    }
}
