package com.layree.codec;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName: JSONEncoder
 * @Author: lin rui
 * @Date: 2023/5/3 21:48
 * @Description:  基于json的序列化实现
 */
public class JSONEncoder implements Encoder{
    @Override
    public byte[] encode(Object obj) {
        return JSON.toJSONBytes(obj);
    }
}
