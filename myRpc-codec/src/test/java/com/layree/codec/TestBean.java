package com.layree.codec;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName: TestBean
 * @Author: lin rui
 * @Date: 2023/5/3 21:51
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestBean {
    private String name;

    private int age;
}
