package com.layree.myrpc.common.utils;

/**
 * @ClassName: TestClass
 * @Author: lin rui
 * @Date: 2023/5/3 21:35
 * @Description:
 */
public class TestClass {

    private String a(){
        return "a";
    }
    public String b(){
        return "b";
    }
    protected String c(){
        return "c";
    }
}
