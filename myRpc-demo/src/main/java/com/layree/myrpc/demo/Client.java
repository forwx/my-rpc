package com.layree.myrpc.demo;

import com.layree.myrpc.client.RpcClient;

/**
 * @ClassName: Client
 * @Author: lin rui
 * @Date: 2023/5/4 18:38
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        RpcClient client = new RpcClient();
        ClientService service = client.getProxy(ClientService.class);

        int r1 = service.add(1, 2);
        int r2 = service.minus(10, 8);

        System.out.println(r1);
        System.out.println(r2);
    }
}
