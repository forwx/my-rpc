package com.layree.myrpc.demo;

/**
 * @ClassName: ClientService
 * @Author: lin rui
 * @Date: 2023/5/4 18:38
 * @Description:
 */
public interface ClientService {
    int add(int a, int b);

    int minus(int a, int b);
}
