package com.layree.myrpc.demo;

/**
 * @ClassName: ClientServiceImpl
 * @Author: lin rui
 * @Date: 2023/5/4 18:39
 * @Description:
 */
public class ClientServiceImpl implements ClientService{
    @Override
    public int add(int a, int b) {
        return a + b;
    }

    @Override
    public int minus(int a, int b) {
        return a - b;
    }
}
