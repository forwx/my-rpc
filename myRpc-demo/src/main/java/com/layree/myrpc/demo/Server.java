package com.layree.myrpc.demo;

import com.layree.myrpc.server.RpcServer;

/**
 * @ClassName: Server
 * @Author: lin rui
 * @Date: 2023/5/4 18:38
 * @Description:
 */
public class Server {

    public static void main(String[] args) {
        RpcServer server = new RpcServer();
        server.register(ClientService.class, new ClientServiceImpl());
        server.start();
    }
}
