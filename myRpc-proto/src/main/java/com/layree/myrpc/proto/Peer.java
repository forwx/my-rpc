package com.layree.myrpc.proto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @ClassName: Peer
 * @Author: lin rui
 * @Date: 2023/5/3 20:49
 * @Description: 表示网络传输的一个断点
 */
@Data
@AllArgsConstructor
public class Peer {
    private String host;
    private int port;
}
