package com.layree.myrpc.proto;

import lombok.Data;

/**
 * @ClassName: Request
 * @Author: lin rui
 * @Date: 2023/5/3 20:53
 * @Description:   表示RPC的一个请求
 */
@Data
public class Request {
    private ServiceDescriptor service;
    private Object[] parameters;
}
