package com.layree.myrpc.proto;

import lombok.Data;

/**
 * @ClassName: Response
 * @Author: lin rui
 * @Date: 2023/5/3 20:55
 * @Description:
 */
@Data
public class Response {
    private int code = 0 ;
    private String msg = "ok";
    private Object data;
}
