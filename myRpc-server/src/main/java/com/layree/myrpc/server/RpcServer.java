package com.layree.myrpc.server;

import com.layree.codec.Decoder;
import com.layree.codec.Encoder;
import com.layree.myrpc.common.utils.ReflectionUtils;
import com.layree.myrpc.proto.Request;
import com.layree.myrpc.proto.Response;
import com.layree.myrpc.transport.RequestHandler;
import com.layree.myrpc.transport.TransportServer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @ClassName: RpcServer
 * @Author: lin rui
 * @Date: 2023/5/4 12:01
 * @Description:
 */
@Slf4j
public class RpcServer {

    private RpcServerConfig config;
    private TransportServer net;
    private Encoder encoder;
    private Decoder decoder;
    private ServerManager serverManager;
    private ServerInvoker serverInvoker;

    public RpcServer(){
        this(new RpcServerConfig());
    }


    public RpcServer(RpcServerConfig config) {
        this.config = config;
        this.net = ReflectionUtils.newInstance(config.getTransportClass());
        this.net.init(config.getPort(), this.handler);
        this.encoder = ReflectionUtils.newInstance(config.getEncoderClass());
        this.decoder = ReflectionUtils.newInstance(config.getDecoderClass());
        this.serverManager = new ServerManager();
        this.serverInvoker = new ServerInvoker();
    }

    public <T> void register(Class<T> interfaceClass, T bean) {
        serverManager.register(interfaceClass, bean);
    }

    public void start() {
        this.net.start();
    }

    public void stop() {
        this.net.stop();
    }

    private RequestHandler handler = new RequestHandler() {
        @Override
        public void onRequest(InputStream receive, OutputStream toResp) {
            //TODO
            Response resp = new Response();
            try {
                byte[] inBytes = IOUtils.readFully(receive, receive.available());
                Request request = decoder.decode(inBytes, Request.class);
                log.info("get request: {}", request);

                ServerInstance sis = serverManager.lookup(request);
                Object ret = serverInvoker.invoke(sis, request);
                resp.setData(ret);

            } catch (Exception e) {
                log.warn(e.getMessage(), e);
                resp.setCode(1);
                resp.setMsg("RpcServer got error: "
                        + e.getClass().getName()
                        + " : " + e.getMessage());
            } finally {
                try {
                    byte[] bytes = encoder.encode(resp);
                    toResp.write(bytes);

                    log.info("response client");
                } catch (IOException e) {
                    log.warn(e.getMessage(), e);
                }
            }
        }
    };
}
