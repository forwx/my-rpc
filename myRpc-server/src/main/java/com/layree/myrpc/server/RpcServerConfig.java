package com.layree.myrpc.server;

import com.layree.codec.Decoder;
import com.layree.codec.Encoder;
import com.layree.codec.JSONDecoder;
import com.layree.codec.JSONEncoder;
import com.layree.myrpc.transport.HttpTransportServer;
import com.layree.myrpc.transport.TransportServer;
import lombok.Data;

/**
 * @ClassName: RpcServerConfig
 * @Author: lin rui
 * @Date: 2023/5/4 10:58
 * @Description:
 */
@Data
public class RpcServerConfig {
    public Class<? extends TransportServer> transportClass = HttpTransportServer.class;
    public Class<? extends Encoder> encoderClass = JSONEncoder.class;
    public Class<? extends Decoder> decoderClass = JSONDecoder.class;

    private int port = 3000;
}
