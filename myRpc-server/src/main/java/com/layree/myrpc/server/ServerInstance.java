package com.layree.myrpc.server;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Method;

/**
 * @ClassName: ServerInstance
 * @Author: lin rui
 * @Date: 2023/5/4 10:58
 * @Description:
 */
@Data
@AllArgsConstructor
public class ServerInstance {
    private Object target;
    private Method method;
}
