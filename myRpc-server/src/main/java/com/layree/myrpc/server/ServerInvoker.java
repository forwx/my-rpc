package com.layree.myrpc.server;

import com.layree.myrpc.common.utils.ReflectionUtils;
import com.layree.myrpc.proto.Request;

/**
 * @ClassName: ServerInvoker
 * @Author: lin rui
 * @Date: 2023/5/4 11:53
 * @Description: 调用具体服务
 */
public class ServerInvoker {
    public Object invoke(ServerInstance service, Request request) {
        return ReflectionUtils.invoke(service.getTarget(),
                service.getMethod(),
                request.getParameters());
    }
}
