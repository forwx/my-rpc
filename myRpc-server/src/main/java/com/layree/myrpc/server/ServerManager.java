package com.layree.myrpc.server;

import com.layree.myrpc.common.utils.ReflectionUtils;
import com.layree.myrpc.proto.Request;
import com.layree.myrpc.proto.ServiceDescriptor;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName: ServerManager
 * @Author: lin rui
 * @Date: 2023/5/4 11:00
 * @Description:
 */
@Slf4j
public class ServerManager {
    private Map<ServiceDescriptor, ServerInstance> servers;

    public ServerManager() {
        this.servers = new ConcurrentHashMap<>();
    }

    public <T> void register(Class<T> interfaceClass, T bean){
        Method[] methods = ReflectionUtils.getPublicMethods(interfaceClass);
        for (Method method : methods) {
            ServerInstance sis = new ServerInstance(bean, method);
            ServiceDescriptor sdp = ServiceDescriptor.from(interfaceClass, method);
            servers.put(sdp, sis);
            log.info("register service: {} {}",sdp.getClazz(), sdp.getMethod());
        }
    }

    public ServerInstance lookup(Request request){
        ServiceDescriptor sdp = request.getService();
        return servers.get(sdp);
    }
}
