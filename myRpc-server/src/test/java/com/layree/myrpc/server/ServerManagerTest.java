package com.layree.myrpc.server;

import com.layree.myrpc.common.utils.ReflectionUtils;
import com.layree.myrpc.proto.Request;
import com.layree.myrpc.proto.ServiceDescriptor;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertNotNull;

public class ServerManagerTest {

    ServerManager sm;

    @Before
    public void init(){
        sm = new ServerManager();

        TestInterface bean = new TestClass();
        sm.register(TestInterface.class, bean);
    }

    @Test
    public void register() {
        TestInterface bean = new TestClass();
        sm.register(TestInterface.class, bean);
    }

    @Test
    public void lookup() {
        Method method = ReflectionUtils.getPublicMethods(TestInterface.class)[0];
        ServiceDescriptor sdp = ServiceDescriptor.from(TestInterface.class, method);

        Request request = new Request();
        request.setService(sdp);

        ServerInstance sis = sm.lookup(request);
        assertNotNull(sis);
    }
}