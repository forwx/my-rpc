package com.layree.myrpc.server;

/**
 * @InterfaceName: TestInterface
 * @Author: lin rui
 * @Date: 2023/5/4 11:43
 * @Description:
 */
public interface TestInterface {
    void hello();
}
