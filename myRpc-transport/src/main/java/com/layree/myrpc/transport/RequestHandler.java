package com.layree.myrpc.transport;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * @ClassName: RequestHandler
 * @Author: lin rui
 * @Date: 2023/5/4 9:15
 * @Description:
 */
public interface RequestHandler {
    void onRequest(InputStream receive, OutputStream toResp);
}
