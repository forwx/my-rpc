package com.layree.myrpc.transport;

import com.layree.myrpc.proto.Peer;

import java.io.InputStream;

/**
 * @ClassName: TransportClient
 * @Author: lin rui
 * @Date: 2023/5/4 9:10
 * @Description:  1.创建连接
 *                2.发送数据，并且等待响应
 *                3.关闭连接
 */
public interface TransportClient {
    void connect(Peer peer);

    InputStream write(InputStream data);

    void close();

}
