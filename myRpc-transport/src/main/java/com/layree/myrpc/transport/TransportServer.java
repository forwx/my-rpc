package com.layree.myrpc.transport;

/**
 * @InterfaceName: TransportServer
 * @Author: lin rui
 * @Date: 2023/5/4 9:13
 * @Description:   1.启动，监听
 *                 2.接受请求
 *                 3.关闭监听
 */
public interface TransportServer {

    void init(int port, RequestHandler handler);
    void start();
    void stop();

}
